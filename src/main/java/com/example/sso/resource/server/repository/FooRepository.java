package com.example.sso.resource.server.repository;

import com.example.sso.resource.server.model.Foo;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface FooRepository extends PagingAndSortingRepository<Foo, Long> {
}
