package com.example.sso.resource.server.web.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class FooDto {

    private long id;
    private String name;
}
