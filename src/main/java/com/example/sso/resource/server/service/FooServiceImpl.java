package com.example.sso.resource.server.service;

import com.example.sso.resource.server.model.Foo;
import com.example.sso.resource.server.repository.FooRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

@Service
@RequiredArgsConstructor
public class FooServiceImpl implements FooService {

    private final FooRepository fooRepository;

    @Override
    public Foo findById(Long id) {
        return fooRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @Override
    public Foo save(Foo foo) {
        return fooRepository.save(foo);
    }

    @Override
    public Iterable<Foo> findAll() {
        return fooRepository.findAll();
    }
}
