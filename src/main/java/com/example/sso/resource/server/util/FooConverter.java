package com.example.sso.resource.server.util;

import com.example.sso.resource.server.model.Foo;
import com.example.sso.resource.server.web.dto.FooDto;
import org.springframework.stereotype.Component;

@Component
public class FooConverter {

    public FooDto fooToDto(Foo foo) {
        return FooDto.builder()
                .id(foo.getId())
                .name(foo.getName())
                .build();
    }

    public Foo dtoToFoo(FooDto fooDto) {
        Foo foo = new Foo(fooDto.getName());
        foo.setId(fooDto.getId());
        return foo;
    }
}
