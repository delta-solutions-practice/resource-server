package com.example.sso.resource.server.service;

import com.example.sso.resource.server.model.Foo;

public interface FooService {

    Foo findById(Long id);

    Foo save(Foo foo);

    Iterable<Foo> findAll();
}
