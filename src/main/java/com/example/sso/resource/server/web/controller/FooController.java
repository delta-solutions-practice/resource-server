package com.example.sso.resource.server.web.controller;

import com.example.sso.resource.server.service.FooService;
import com.example.sso.resource.server.util.FooConverter;
import com.example.sso.resource.server.web.dto.FooDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@RestController
@RequestMapping("/api/foos")
@RequiredArgsConstructor
public class FooController {

    private final FooService fooService;
    private final FooConverter fooConverter;

    @GetMapping("/{id}")
    public FooDto findById(@PathVariable Long id) {
        return fooConverter.fooToDto(fooService.findById(id));
    }

    @GetMapping
    public Collection<FooDto> findAll() {
        return StreamSupport.stream(fooService.findAll().spliterator(), false)
                .map(fooConverter::fooToDto)
                .collect(Collectors.toList());
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void save(@RequestBody FooDto foo) {
        fooService.save(fooConverter.dtoToFoo(foo));
    }
}
